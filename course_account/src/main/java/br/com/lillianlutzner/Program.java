package br.com.lillianlutzner;

import entities.Account;
import entities.BusinessAccount;
import entities.SavingsAccount;

public class Program {

	public static void main(String[] args) {
		
		Account acc = new Account(1001, "Lillian", 0.0);
		BusinessAccount bacc = new BusinessAccount(1002, "André", 0.0, 500.0);
		
		// Upcasting
		
		Account acc1 = bacc;
		Account acc2 = new BusinessAccount(1003, "Maria", 0.0, 100.0);
		Account acc3 = new SavingsAccount(1004, "Benjamim", 0.0, 0.01);
		
		System.out.println(acc1.getHolder());
		System.out.println(acc2.getHolder() + " " + acc2.getNumber());
		
		// Downcasting
		
		BusinessAccount bacc2 = (BusinessAccount) acc2; // preciso fazer um casting manualmente
		bacc2.loan(100.0);
		
		Account acc4 = new Account(1005, "Marlene", 1000.0);
		acc4.withdraw(200.0);
		System.out.println(acc4.getBalance());
		
		Account sacc = new SavingsAccount(1006, "Bernd", 1000.0, 0.01);
		sacc.withdraw(200.0);
		System.out.println(sacc.getBalance());
		
		Account bacc3 = new BusinessAccount(1007, "Sascha", 1000.0, 500.0);
		bacc3.withdraw(200);
		System.out.println(bacc3.getBalance());
		
	}

}
